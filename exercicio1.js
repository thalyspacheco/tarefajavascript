// Implemente um algoritmo que pegue duas matrizes(array de arrays) e
// realize sua multiplicação.Lembrando que para realizar a multiplicação
// dessas matrizes o número de colunas da primeira matriz tem que ser
// igual ao número de linhas da segunda matriz. (2x2)

// Retorna um número aleatório entre 1 e 10
function rand() {
    return Math.floor(Math.random() * 10) + 1
}
// Imprime a Matriz na tela
function showMatrix(nome, matriz) {
    console.log('\nMatriz', nome)
    for (let i = 0; i < 2; i++) {
        console.log(matriz[i])
    }
}

//Criando Matrizes vazias
var matrizA = [[null, null],[null, null]]
var matrizB = [[null, null],[null, null]]
var matrizResultante = [[null, null],[null, null]]

// Definindo valores aleatórios para os elementos das Matrizes
for (i = 0; i < 2; i++) {
    for (j = 0; j < 2; j++) {
        matrizA[i][j] = rand()
        matrizB[i][j] = rand()
    }
}

showMatrix('A', matrizA)
showMatrix('B', matrizB)

//Multiplicando as Matrizes
for (i = 0; i < 2; i++) {
    for (j = 0; j < 2; j++) {
        for (k = 0; k < 2; k++) {
            matrizResultante[i][j] += matrizA[i][k] * matrizB[k][j]
        }
    }
}

showMatrix('Resultante', matrizResultante)