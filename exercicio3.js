// Retorna um número aleatório entre 1 e 10
function rand() {
    return Math.floor(Math.random() * 10) + 1
}

var nota1 = rand()
var nota2 = rand()
var nota3 = rand()

console.log('O aluno obteve as seguintes notas:',nota1,',',nota2,'e',nota3)

if ((nota1 + nota2 + nota3) / 3 >= 6) {
    console.log('Aprovado!')
} else {
    console.log('Reprovado!')
}