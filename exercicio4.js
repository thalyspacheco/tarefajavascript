import gods from "./arquivo_exercicio_4.js"

//Q1
gods.forEach(deuses => {
  console.log(deuses.name, deuses.features.length)
});

//Q2
gods.filter(deuses => {
  if (deuses.roles == "Mid") {
    console.log(deuses)
  }
});

//Q3
gods.sort((a, b) => (a.pantheon > b.pantheon) ? 1 : -1)
console.log(gods)

//Q4
var newGods = []
gods.forEach(deuses => {
  newGods.push(deuses.name + ' (' + deuses.class + ')' )
});
console.log(newGods)