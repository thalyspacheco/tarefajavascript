// Retorna um número aleatório entre 1 e 100
function rand() {
    return Math.floor(Math.random() * 100) + 1
}

for (i = 0; i < 5; i++) {
    var value = rand()
    if (value % 3 == 0 && value % 5 == 0) {
        console.log(value,'fizzbuzz')
    } else if (value % 3 == 0) {
        console.log(value,'fizz')
    } else if (value % 5 == 0) {
        console.log(value,'buzz')
    } else {
        console.log(value)
    }
}
